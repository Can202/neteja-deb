# neteja-deb

Paquete de Debian (deb) para neteja

# Descargar deb
Revise la última release: https://gitlab.com/Can202/neteja-deb/-/releases

## Nejeta
Neteja es un script de Terminal para limpieza del sistema que soporta un flatpak, snap, y apt.
Puedes limpiar el Sistema, borrando caché, actualizar el sistema y/o vaciar la papelera.

### Link del Proyecto Nejeta: https://gitlab.com/voromv/Neteja
